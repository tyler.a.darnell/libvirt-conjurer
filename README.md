# Conjurer

Conjurer is a Flask application intended to manage KVM guests using libvirt. Conjurer should be run from the hypervisor machine itself.


# Disclaimer

Conjurer is provided as-is with no warranty. It is not for use in production. Conjurer could allow users with access to its interface the ability to run arbitrary code in a VM on your machine. That could be bad.
