import libvirt
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from conjurer.models.domains import StateType, Domain, lv_to_model_state
from conjurer.models import db

bp = Blueprint('domains', __name__, url_prefix="/domains")

@bp.route('/')
def list_libvirt_domains():
    """
    Ensures DB status matches libvirt state then lists domains
    """

    db_match_state()
    domainList = Domain.query.all()

    # domainNameList = []
    # for domain in domList:
    #    domainNameList.append(domain.name())

    # Need to do processing on this side and pass object for display in
    # E.g. to catch execption when you try to grab the hostname and your guest
    # VM does not have qemu-guest-agent running

    return render_template('domains/list.html', domains=domainList)

@bp.route('/<int:domain_id>')
def show_domain(domain_id):
    try:
        conn = libvirt.open('qemu:///system')
    except libvirt.libvirtError:
        return 'Failed to open connection to the hypervisor'

    return 'Stub'


### logic only functions below here
# DB state management functions
#
# The goal is to have the DB state reflect the state of all domains (stopped, paused, running)
# on the hypervisor at any given time. This requires a set of update functions
###

### Errors
class NonUniqueDomainError(Exception):
    pass

class RecordNotFoundError(Exception):
    pass

###
# DB manipulation functions
##

def db_add_domain(uuid, os, name, state=StateType.UNDEFINED):
    """
    Adds a domain to the DB
    Arguments:
    uuid: libvirt UUID for the domain
    os: os value for the domain
    name: name for the domain (REQUIRED not null)
    state: state of the domain (defaults to Undefined)
    Succeeds if no other domain with the same UUID is already in the database
    """
    if Domain.query.filter_by(uuid=uuid).count() >= 1:
        raise NonUniqueDomainError
    else:
        new_domain = Domain(uuid=uuid, os=os, name=name, state=state)
        db.session.add(new_domain)
        db.session.commit()

def db_update_domain(uuid, os=None, name=None, state=None):
    """
    Update domain selected using UUIDString

    Arguments:
    - uuid (required): uuid of domain to update
    - os (optional): OS Type from libvirt
    - name (optional): name of the domain
    - state (optional): state of domain
    """
    domain = Domain.query.filter(Domain.uuid==uuid).first()
    if not domain:
        raise RecordNotFoundError

    if os:
        domain.os = os
    if name:
        domain.name = name
    if state:
        domain.state = state

    db.session.commit()

def db_match_state():
    """
    Reads current list of libvirt domains from API and ensures DB state matches

    for each domain via libvirt
        if UUID not in DB
            add domain
        else
            update domain state

    for each domain in DB
        if UUID not in libvirt list
            update domain state to undefined
    """
    try:
        conn = libvirt.open('qemu:///system')
    except libvirt.libvirtError:
        return 'Failed to open connection to the hypervisor'

    try:
        domList = conn.listAllDomains(0)
    except libvirt.libvirtError:
        return 'Failed to list domains.'

    for domain in domList:
        db_match = Domain.query.filter_by(uuid=domain.UUIDString()).all()
        state = lv_to_model_state(domain.state())
        if len(db_match) < 1:
            db_add_domain(domain.UUIDString(), domain.OSType(), domain.name(), state)
        else:
            db_update_domain(domain.UUIDString(), domain.OSType(), domain.name(), state)

    dbDomList = Domain.query.all()
    for domain in dbDomList:
        pass
