from conjurer.models import db
import enum
import libvirt

class StateType(enum.Enum):
    """
    VM lifecycle states
    Match the states shown on this libvirt wiki page:
    https://wiki.libvirt.org/page/VM_lifecycle
    """
    UNDEFINED="Undefined"
    RUNNING="Running"
    STOPPED="Stopped"
    PAUSED="Paused"
    SAVED="Saved"
    UNKNOWN="Unknown"

def lv_to_model_state(state):
    if state == libvirt.VIR_DOMAIN_NOSTATE:
        return StateType.UNDEFINED
    elif state == libvirt.VIR_DOMAIN_RUNNING:
        return StateType.RUNNING
    elif state == libvirt.VIR_DOMAIN_PAUSED:
        return StateType.PAUSED
    else:
        return StateType.UNKNOWN

class Domain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(128), unique=True)
    os = db.Column(db.String(128))
    name = db.Column(db.String(128), nullable=False)
    state = db.Column(db.Enum(StateType), nullable=False)
