import pytest
from conjurer.domains import db_add_domain, db_update_domain, db_match_state
from conjurer.models.domains import Domain, StateType

# this test is written to pass when 2 domains are running per test-machines-up.sh
def test_list_default(client):
    rv = client.get("/domains/")
    assert b'test' in rv.data

@pytest.mark.xfail
def test_view_one(client):
    rv = client.get("/domains/1/")
    assert b'test 1' in rv.data

def test_add_domain_db(client, app):
    with app.app_context():
        db_add_domain("test_uuid_1", "Linux amd64", "test name 1")
        db_add_domain("test_uuid_2", "Linux amd64", "test name 2", StateType.RUNNING)
        db_domains_list = Domain.query.all()
    assert len(db_domains_list) == 2

def test_match_state(app, client):
    with app.app_context():
        db_match_state()
        row_list = Domain.query.all()
    assert len(row_list) == 2

def test_update_domain(client, app):
    with app.app_context():
        # db_update_domain(domain.UUIDString(), domain.OSType(), domain.name(), state)
        db_add_domain("test_uuid_1", "Linux amd64", "test name 1", StateType.RUNNING)
        db_update_domain("test_uuid_1", state=StateType.PAUSED)
        db_domain = Domain.query.filter(Domain.uuid == "test_uuid_1").first()
    assert db_domain.state == StateType.PAUSED
